# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR The Claws Mail Team
# This file is distributed under the same license as the PACKAGE package.
# Páder Rezső <rezso@rezso.net>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: quitcount 1.6\n"
"Report-Msgid-Bugs-To: colin@colino.net\n"
"POT-Creation-Date: 2018-10-23 09:39+0200\n"
"PO-Revision-Date: 2009-07-09 19:41+0100\n"
"Last-Translator: rezso <rezso@rezso.net>\n"
"Language-Team:  <NONE>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Hungarian\n"
"X-Poedit-Country: HUNGARY\n"

#: src/main.c:210
#, c-format
msgid "Life expectancy gain: %d day!"
msgid_plural "Life expectancy gain: %d days!"
msgstr[0] "Várható élettartam növekedés: %d nap!"
msgstr[1] "Várható élettartam növekedés: %d nap!"

#: src/main.c:213
#, c-format
msgid "Life expectancy gain: %d week!"
msgid_plural "Life expectancy gain: %d weeks!"
msgstr[0] "Várható élettartam növekedés: %d hét!"
msgstr[1] "Várható élettartam növekedés: %d hét!"

#: src/main.c:216
#, c-format
msgid "Life expectancy gain: %d month!"
msgid_plural "Life expectancy gain: %d months!"
msgstr[0] "Várható élettartam növekedés: %d hónap!"
msgstr[1] "Várható élettartam növekedés: %d hónap!"

#: src/main.c:219
#, c-format
msgid "Life expectancy gain: %d year!"
msgid_plural "Life expectancy gain: %d years!"
msgstr[0] "Várható élettartam növekedés: %d év!"
msgstr[1] "Várható élettartam növekedés: %d év!"

#: src/main.c:224 src/main.c:227 src/main.c:230 src/main.c:233
#, c-format
msgid "%d %s saved!"
msgstr "%d %s megtakarítás!"

#: src/main.c:237
#, c-format
msgid "You quit since %d week!"
msgid_plural "You quit since %d weeks!"
msgstr[0] "%d hete hagytad abba!"
msgstr[1] "%d hete hagytad abba!"

#: src/main.c:244
#, c-format
msgid "You quit since %d month!"
msgid_plural "You quit since %d months!"
msgstr[0] "%d hónapja hagytad abba!"
msgstr[1] "%d hónapja hagytad abba!"

#: src/main.c:249
#, c-format
msgid "You quit since %d year!"
msgid_plural "You quit since %d years!"
msgstr[0] "%d éve hagytad abba!"
msgstr[1] "%d éve hagytad abba!"

#: src/main.c:252 src/main.c:255
#, c-format
msgid "You quit since %d days!"
msgstr "%d napja hagytad abba!"

#: src/main.c:337
msgid "QuitCount configuration"
msgstr "QuitCount beállítás"

#: src/main.c:349
msgid "I quit on"
msgstr "Abbahagytam:"

#: src/main.c:363
msgid "I smoked"
msgstr "Elszívtam"

#: src/main.c:372
msgid "cigarettes per day"
msgstr "cigarettát naponta"

#: src/main.c:381
msgid "Each contained"
msgstr "Mindegyik tartalmazott"

#: src/main.c:390
msgid "mg of nicotine"
msgstr "mg nikotint"

#: src/main.c:404
msgid "mg of tar"
msgstr "mg kátrányt"

#: src/main.c:413
msgid "It cost"
msgstr "Ára:"

#: src/main.c:429
msgid "per pack of"
msgstr "csomagonként ennyi szállal:"

#: src/main.c:442
msgid "<span size='x-large'><b>QuitCount - configuration</b></span>"
msgstr "<span size='x-large'><b>QuitCount - beállítás</b></span>"

#: src/main.c:495
#, fuzzy
msgid ""
"Copyright (C) 2009-2013, Colin Leroy <colin@colino.net>\n"
"GPL version 3 or later"
msgstr ""
"Copyright (C) 2009, Colin Leroy <colin@colino.net>\n"
"GPL 3 vagy későbbi verzió"

#: src/main.c:522
msgid "QuitCount statistics"
msgstr "QuitCount statisztikák"

#: src/main.c:544
msgid "<span size='large'>days since I quit</span>"
msgstr "<span size='large'>napja hagytam abba</span>"

#: src/main.c:562
#, c-format
msgid "%d year"
msgid_plural "%d years"
msgstr[0] "%d év"
msgstr[1] "%d év"

#: src/main.c:564
#, c-format
msgid "%d month"
msgid_plural "%d months"
msgstr[0] "%d hónap"
msgstr[1] "%d hónap"

#: src/main.c:566
#, c-format
msgid "%d week"
msgid_plural "%d weeks"
msgstr[0] "%d hét"
msgstr[1] "%d hét"

#: src/main.c:597
msgid "<span size='large'>cigarettes not smoked</span>"
msgstr "<span size='large'>nem elszívott cigaretta</span>"

#: src/main.c:608 src/main.c:617
msgid "Kg"
msgstr "kg"

#: src/main.c:611 src/main.c:620
msgid "g"
msgstr "g"

#: src/main.c:614 src/main.c:623
msgid "mg"
msgstr "mg"

#: src/main.c:625
#, c-format
msgid ""
"Nicotine: %.2f %s\n"
"Tar: %.2f %s"
msgstr ""
"Nikotin: %.2f %s\n"
"Kátrány: %.2f %s"

#: src/main.c:642
#, c-format
msgid "<span size='large'>%s saved</span>"
msgstr "<span size='large'>%s megtakarítás</span>"

#: src/main.c:654
msgid "year"
msgid_plural "years"
msgstr[0] "év"
msgstr[1] "év"

#: src/main.c:657
msgid "month"
msgid_plural "months"
msgstr[0] "hónap"
msgstr[1] "hónap"

#: src/main.c:660
msgid "week"
msgid_plural "weeks"
msgstr[0] "hét"
msgstr[1] "hét"

#: src/main.c:663
msgid "day"
msgid_plural "days"
msgstr[0] "nap"
msgstr[1] "nap"

#: src/main.c:666
msgid "hour"
msgid_plural "hours"
msgstr[0] "óra"
msgstr[1] "óra"

#: src/main.c:668
msgid "minute"
msgid_plural "minutes"
msgstr[0] "perc"
msgstr[1] "perc"

#: src/main.c:677
msgid "<span size='large'>of life expectancy gained</span>"
msgstr "<span size='large'>várható élettartam növekedés</span>"

#: src/main.c:686
msgid "<span size='x-large'><b>QuitCount - statistics</b></span>"
msgstr "<span size='x-large'><b>QuitCount - statisztikák</b></span>"

#: src/main.c:724
msgid "Statistics"
msgstr "Statisztikák"

#~ msgid "Life expectancy gain: %d hour!"
#~ msgid_plural "Life expectancy gain: %d hours!"
#~ msgstr[0] "Várható élettartam növekedés: %d óra!"
#~ msgstr[1] "Várható élettartam növekedés: %d óra!"
